+++
date = "2016-11-06T13:00:25+05:30"
title = "Contact Us"
weight = 19
+++

INDIA :
Sanket Italiya
+91 9033519642
contact@italiyapackaging.com 

GERMANY :
Bhavin Khadela
+49 17645695874
Khadela@italiyapackaging.com

KUWAIT :
Nikhil Changmai
+965 55303281
n.changmai@italiyapackaging.com

Let's get in touch.

Contact us for all your packagining requirements.