---
title: "PP/HDPE bags"
date: 2018-03-06T16:01:23+08:00
lastmod: 2018-03-07T16:01:23+08:00
draft: false
tags: ["preview", "shortcodes", "tutorial"]
categories: ["Notes"]
authors:
- "Shaform"
- "Jan Doe"
---


<!--more-->

## Polypropylene(PP)/High Density Poly Ethylene Bags(HDPE)

We provide PP/HDPE bag with capacity 10 Kg to 50 Kg also known as sand bag with customize size, printing, with/without liner with nylon stereo printing. 



## Images


![PP](.png)
