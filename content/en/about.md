---
title: "About"
date: 2017-08-20T21:38:52+08:00
lastmod: 2017-08-28T21:41:52+08:00
menu: "main"
weight: 50
---


Italiya Packaging Solutions Activities has been established as a Packaging company in West of India. Our traditional business model is based on the Supply packaging material and packaging tool in Worldwide market. 

#### Company Strategy
Purpose to be a leader in the packaging industry by providing enhanced services, relationship and profitability.

#### Vision
To provide quality services that exceeds the expectations of our esteemed customers.

#### Mission
To build long term relationships with our customers and clients and provide exceptional customer services by pursuing business through innovation and advanced technology.

![This is me][1]

#### Core values
* We believe in treating our customers with respect and faith.
* We grow through creativity, invention and innovation.
* We integrate honesty, integrity and business ethics into all aspects of our business functioning Goals.
* International expansion in the field of packaging and develop a strong base of key customers.
* Increase the assets and investments of the company to support the development of services.
* To build good reputation in the field of packaging and become a key player in the packaging solutions.

#### Scope of Work
Italiya Packaging Solutions provide material for packaging & tooling of multinational companies. The company undertakes all packaging duties for all relevant product packing, fragile material packing, solid & liquid row material packing, fine finished product packing, and many more daily packing materials.


[1]: /img/about.jpg


* [Cobra](https://github.com/spf13/cobra)
* [Viper](https://github.com/spf13/viper)
* [J Walter Weatherman](https://github.com/spf13/jWalterWeatherman)
* [Cast](https://github.com/spf13/cast)

Learn more and contribute on [GitHub](https://github.com/gohugoio).
